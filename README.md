# bcwk2web

Web app for bcwk2. Python Web App running in Docker.

basic python web app run from http://localhost:8000/

### list built images 
docker image ls

### build image
docker build . -t pyserv --no-cache

### delete image 
docker image rm -f NNNNNNNNNNNNN

### log on to image
docker exec -it [image name] bash

### run image oublishing port 8000
docker run -p 8000:8000 -t [image tag]

## stop a container 
docker stop [name]

## push image to dockerhub
docker push svvatcap/pyserv